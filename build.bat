IF NOT EXIST paket.lock (
    START /WAIT .paket/paket.exe install
)
dotnet restore src/fsharp
dotnet build src/fsharp

dotnet restore tests/fsharp.Tests
dotnet build tests/fsharp.Tests
dotnet test tests/fsharp.Tests
