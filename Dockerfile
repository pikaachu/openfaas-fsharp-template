FROM openfaas/of-watchdog:0.7.2 as watchdog

FROM fsharp:netcore

COPY --from=watchdog /fwatchdog /usr/bin/fwatchdog
RUN chmod +x /usr/bin/fwatchdog


ARG ADDITIONAL_PACKAGE
RUN apt-get update \
  && apt-get install -qy --no-install-recommends ${ADDITIONAL_PACKAGE}

WORKDIR /home/app

COPY function .
COPY build-prod.sh .
COPY .paket .
COPY paket-files .
COPY src .
COPY paket.dependencies . 

RUN chmod +x build-prod.sh





