module Tests

open System
open System.IO
open System.Net
open System.Net.Http
open Xunit
open Microsoft.AspNetCore.Builder
open Microsoft.AspNetCore.Hosting
open Microsoft.AspNetCore.TestHost
open Microsoft.Extensions.DependencyInjection
open Newtonsoft.Json
open FSharp.Control.Tasks.V2.ContextInsensitive

// ---------------------------------
// Helper functions (extend as you need)
// ---------------------------------

let createHost() =
    WebHostBuilder()
        .UseContentRoot(Directory.GetCurrentDirectory())
        .Configure(Action<IApplicationBuilder> fsharp.App.configureApp)
        .ConfigureServices(Action<IServiceCollection> fsharp.App.configureServices)

let runTask task =
    task
    |> Async.AwaitTask
    |> Async.RunSynchronously

let httpGet (path : string) (client : HttpClient) =
    path
    |> client.GetAsync
    |> runTask

let isStatus (code : HttpStatusCode) (response : HttpResponseMessage) =
    Assert.Equal(code, response.StatusCode)
    response

let ensureSuccess (response : HttpResponseMessage) =
    if not response.IsSuccessStatusCode
    then response.Content.ReadAsStringAsync() |> runTask |> failwithf "%A"
    else response

let readMessage (response : HttpResponseMessage) =
    task {
        let! stringRes = response.Content.ReadAsStringAsync()
        return JsonConvert.DeserializeObject<{| Message: string |}>(stringRes)
    } |> runTask

let shouldEqual expected actual =
    Assert.Equal(expected, actual)

let shouldContainMessage (actual : string) (x: {| Message: string |})=
    Assert.True(actual.Contains x.Message)

// ---------------------------------
// Tests
// ---------------------------------

// CHANGE THIS TEST TO MATCH YOUR FUNCTION AND ADD MORE TESTS IF YOU WANT
[<Fact>]
let ``Route / returns JSON message`` () =
    use server = new TestServer(createHost())
    use client = server.CreateClient()

    client
    |> httpGet "/"
    |> ensureSuccess
    |> readMessage
    |> shouldContainMessage "Hello"

