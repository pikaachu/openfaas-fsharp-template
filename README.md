# OpenFaaS :heart: F#

This is a template to create OpenFaaS functions in F#. This uses the `of-watchdog` configuration and
it runs using the HTTP mode for more performant code.

This also allows you to persist connections like SQL connections.
