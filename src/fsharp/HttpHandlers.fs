namespace fsharp

module HttpHandlers =

    open Microsoft.AspNetCore.Http
    open FSharp.Control.Tasks.V2.ContextInsensitive
    open Giraffe
    open fsharp.Models
    open fsharp.Handler

    let handle (next : HttpFunc) (ctx : HttpContext) =
        task {
            let! result = handlerFn ctx.Request
            return! json result next ctx
        }
