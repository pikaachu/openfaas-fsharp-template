namespace fsharp.Models

[<CLIMutable>]
type Message =
    {
        Text : string
    }