namespace fsharp

module Handler = 
    open FSharp.Control.Tasks.V2.ContextInsensitive
    open Microsoft.AspNetCore.Http

    // handler function
    let handlerFn (req : HttpRequest) =
        task {
            return {| Message = "Hello" |}
        }