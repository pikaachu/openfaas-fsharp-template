#!/bin/sh
if [ ! -e "paket.lock" ]
then
    exec mono .paket/paket.exe install
fi
dotnet restore src/fsharp
dotnet build src/fsharp

dotnet restore tests/fsharp.Tests
dotnet build tests/fsharp.Tests
dotnet test tests/fsharp.Tests
